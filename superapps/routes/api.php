<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::prefix('dev/{cid}')->group(function () {
    Route::get('/resources','App\Http\Controllers\ResourcesController@showall');
    Route::get('/{id}/resources','App\Http\Controllers\ResourcesController@show');
    Route::post('/resources','App\Http\Controllers\ResourcesController@add_resources');
    Route::put('/resources/{id}', 'App\Http\Controllers\ResourcesController@edit_fee');
    Route::delete('/resources/{id}', 'App\Http\Controllers\ResourcesController@delete');

    Route::get('/fee','App\Http\Controllers\FeeController@showall');
    Route::get('/{id}/fee','App\Http\Controllers\FeeController@show');
    Route::post('/fee','App\Http\Controllers\FeeController@add_fee');
    Route::put('/fee/{id}', 'App\Http\Controllers\FeeController@edit_fee');
    Route::delete('/fee/{id}', 'App\Http\Controllers\FeeController@delete');
});

