<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;
    protected $table = "client";
    public $incrementing = true;
    public $timestamps = true;
    protected $guarded = [];

    public function corporate() {
        return $this->hasMany('App\Corporate')->whereNull('deleted_at');
    }

    public function member() {
        return $this->hasMany('App\Member')->whereNull('deleted_at');
    }

    public function user() {
        return $this->belongsTo('App\User');
    }

    public static function getIDClient($user_id) {
        $client = Client::where('user_id', $user_id)->firstOrFail();
        return $client->id;
    }

    public static function getIDClientFromURL($url_client) {
        $client = Client::where('id_url', $url_client)->firstOrFail();
        return $client->id;
    }
}
