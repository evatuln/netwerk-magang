<?php

namespace App\Http\Controllers;

use App\Models\Fee;
use App\Client;
use App\Models\Resources;
use Illuminate\Http\Request;

class FeeController extends Controller
{
    


    public function add_fee(Request $request, $cid) {
        $status = "error";
        $message = "Error while add data";
        $code = 500;

        if($cid != "all") {
            $client_id = Client::getIDClientFromURL($cid);
            if(!$client_id) {
                return response()->json([
                    'status' => false,
                    'message' => "Error client_id not found!"
                ], $code);
            }
        }

        $feeadd = Fee::create([
            'name' => $request->post('name'),
            'description' => $request->post('description'),
        ]);

        if($feeadd) {
            $status = true;
            $message = "Success";
            $code = 200;
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
        ], $code);
    }



    public function show( $cid, $id  ) {
        $status = "error";
        $message = "Error while getting data";
        $data = Resources::findOrFail($id);

        if($cid != "all") {
            $client_id = Client::getIDClientFromURL($cid);
            if(!$client_id) {
                return response()->json([
                    'status' => false,
                    'message' => "Error client_id not found!"
                ], 200);
            }
        }

        if($id == Resources::findOrFail($id)) {
            $fee = Fee::with(['client']);
            $fee = $fee->get();
            
            if($fee) {
                $status = "success";
                $message = "Get data Category successfully";
                $code = 200;
                $data = $fee;
            }
        } 

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ], 200);
    }

    public function showall(Fee $fee, $cid)
    {    
        $status = "true";
        $message = "succes";
        $data = Fee::all();
        $code = 500;

        if($cid != "all") {
            $client_id = Client::getIDClientFromURL($cid);
            if(!$client_id) {
                return response()->json([
                    'status' => false,
                    'message' => "Error client_id not found!"
                ], 200);
            }
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
            'data' => $data
        ], 200);
    }

    public function edit_fee(Request $request, $cid, $id) {
        $status = "error";
        $message = "Error while edit data";
        $data = NULL;
        $code = 500;

        if($cid != "all") {
            $client_id = Client::getIDClientFromURL($cid);
            if(!$client_id) {
                return response()->json([
                    'status' => false,
                    'message' => "Error client_id not found!"
                ], 200);
            }
        }

        $fee = Fee::find($id);
        $fee->name = $request->input('name');
        $fee->description = $request->input('description');
        $fee->save();

        if($fee) {
            $status = true;
            $message = "Success";
            $code = 200;
        }

        return response()->json([
            'status' => $status,
            'message' => $message,
        ], $code);
    }


    public function delete($cid, $id) {
        $status = false;
        $message = "Error while delete data Fee";
        $code = 200;

        if($cid != "all") {
            $client_id = Client::getIDClientFromURL($cid);
            if(!$client_id) {
                return response()->json([
                    'status' => false,
                    'message' => "Error client_id not found!"
                ], 200);
            }
        }

        $fee = Fee::find($id);
        $fee->delete();

        if($fee) {
            $status = true;
            $message = "Success";
            $code = 200;
        }

        return response()->json([
            'status' => $status,
            'message' => $message
        ], $code);
    }




   
    public function update(Request $request, Fee $fee)
    {
        //
    }

  
    public function destroy(Fee $fee)
    {
        //
    }
}
