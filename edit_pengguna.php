<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, viewport-fit=cover, user-scalable=no, shrink-to-fit=no" />
        
        <link rel="stylesheet" href="{{ asset('dbc/assets/css/bootstrap.css') }}" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css" />
        <link rel="stylesheet" href="{{ asset('dbc/assets/css/bootstrap-datetimepicker.css') }}" />
        <link rel="stylesheet" href="{{ asset('dbc/assets/css/bootstrap-datetimepicker-standalone.css') }}" />
        <link rel="stylesheet" href="{{ asset('dbc/assets/css/style.css') }}" />

        <link rel="icon" href="dbc/assets/images/Logo2.png" type="image/x-icon">
        <title>TAG | Stay Connected</title>
    </head>
    <body>
        @foreach($member as $data)
        <div class="container-mobile">
            <div class="background">
                <div class="logo">
                    <img src="{{ asset('dbc/assets/images/logo.svg') }}" class="img-fluid" />
                </div>
                
                <div class="profile">
                    @if($data->username == "mlee")
                        <div class="profile__image rounded-circle"><img src="assets/images/mlee.svg" class="img-fluid"/></div>
                    @else
                        <div class="profile__image rounded-circle"><img src="assets/images/profile.svg" class="img-fluid"/></div>
                    @endif    
                    <div class="profile__name">
                        <div class="profile__name-size">{{ $data->nama }}</div>
                    </div>
                    <div class="profile__position">{{ $data->profile }}</div>
                    <div class="button button--chat">
                        Chat Me <img src="{{ asset('dbc/assets/images/icon-chat.svg') }}" class="img-fluid" />
                    </div>
                    <div class="button button--room">Enter Room</div>
                </div>
                
                <div class="information">
                    <div class="information__top-bar" data-action="top">
                        <div class="information__pin-bar"></div>
                    </div>
                    <div class="information__body-bar d-none">
                        <div class="information__body-bar-content">
                            <div class="information__company">
                                <div class="information__company-name">{{ $data->company_name }}</div>
                                <div class="information__company-description">{{ $data-> company_description }}</div>
                                <ul class="information__company-contact">
                                    <li>W : <a href="">{{ $data->company_contact_website }}</a></li>
                                    <li>P : {{ $data->company_contact_phone }}</li>
                                    <li>T : {{ $data->company_contact_mobile }}</li>
                                </ul>
                            </div>
                            <div class="information__about-me">
                                <div class="information__about-me-title">About Me</div>
                                <div class="information__about-me-contact">
                                    <div class="d-flex">
                                        <div>Name</div>
                                        <div>:</div>
                                        <div>{{ $data->nama }}</div>
                                    </div>
                                    <div class="d-flex">
                                        <div>Title</div>
                                        <div>:</div>
                                        <div>{{ $data->profile }}</div>
                                    </div>
                                    <div class="d-flex">
                                        <div>Email</div>
                                        <div>:</div>
                                        <div>{{ $data->email }}</div>
                                    </div>
                                    <div class="d-flex">
                                        <div>Phone</div>
                                        <div>:</div>
                                        <div>{{ $data->company_contact_mobile }}</div>
                                    </div>
                                </div>
                                <div class="d-flex">
                                    <a href="" target="_blank">
                                        <div class="information__button-contact information__button-contact--ig rounded-circle">
                                            <img src= "{{ asset('dbc/assets/images/icon-instagram.svg') }}" class="img-fluid" />
                                        </div>
                                    </a>
                                    <a href="" target="_blank">
                                        <div class="information__button-contact information__button-contact--ig rounded-circle">
                                            <img src= "{{ asset('dbc/assets/images/icon-linkedin.svg" class="img-fluid') }}" />
                                        </div>
                                    </a>
                                    <a href="" target="_blank">
                                        <div class="information__button-contact information__button-contact--ig rounded-circle">
                                            <img src= "{{ asset('dbc/assets/images/icon-line.svg') }}" class="img-fluid" />
                                        </div>
                                    </a>
                                    <a id="share-button" href="javascript:void(0);">
                                        <div class="information__button-contact information__button-contact--ig rounded-circle">
                                            <img src= "{{ asset('dbc/assets/images/icon-share.svg') }}" class="img-fluid" />
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="information__office">
                                <div class="information__office-title">
                                    Office
                                </div>
                                <div class="information__office-list">
                                    HQ/Production Office
                                    <div class="information__office-list-address">
                                        Pandanwangi Green Park B-1
                                        Jl. Simpang LA. Sucipto
                                        Malang, Indonesia 65126
                                    </div>
                                </div>
                                <div class="information__office-list">
                                    Operational Office
                                    <div class="information__office-list-address">
                                        Jl. Hayam Wuruk No 3HH
                                        Kebon Kelapa, Gambir
                                        Jakarta Pusat, Indonesia 10120
                                    </div>
                                </div>
                            </div>
                            <div class="footer">
                                Stay Connected with TAG | <a href="https://tagid.ai/#pricing">Create yours!</a>
                            </div>
                        </div>
                        <div class="information__qr-code d-none">
                            <img src="{{ asset('dbc/assets/images/qr-code.svg') }}" class="img-fluid" />
                            <div>or Share With Link :</div>
                            <div class="information__qr-code-link">gg.gg/stephaniegan</div>
                        </div>
                        <div class="information__button-bottom">
                            <div class="information__button-bottom-popup">
                                <div class="button button--save-contact">
                                    <img src="{{ asset('dbc/assets/images/icon-contact.svg') }}" class="img-fluid" /> Save Contact
                                </div>
                                <div class="button button--visit-web">
                                    <img src="{{ asset('dbc/assets/images/icon-visit.svg') }}" class="img-fluid" /> Visit Web
                                </div>
                            </div>
                            <div class="information__button-bottom-bg d-flex">
                                <div id="schedule-meeting" class="button button--schedule-meeting">Schedule a Meeting</div>
                                <div class="information__button-bottom-grey rounded-circle" data-active="1">
                                    <img class="information__dot dot-first" src="{{ asset('dbc/assets/images/dot-menu.svg') }}" />
                                    <img class="information__dot dot-second" src="{{ asset('dbc/assets/images/dot-menu.svg') }}" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="meeting d-none">
                <div class="logo logo--black">
                    <img src="{{ asset('dbc/assets/images/logo-black.svg') }}" class="img-fluid" />
                </div>
                <form method="post">
                    <div class="meeting__title">
                        Schedule a Meeting
                    </div>
                    <div class="form-group form__input">
                        <label for="topic" class="form__input-label">Topic</label>
                        <input type="text" class="form-control form__input-tag" id="topic" name="topic" placeholder="Topic Meeting">
                    </div>
                    <div class="form-group form__input">
                        <div class="d-flex">
                            <label for="date-from" class="form__input-label">Date</label>
                            <div class="flex-fill form__input-label--time-zone">Time Zone : Jakarta (GMT+7)</div>
                        </div>
                        <div class="d-flex">
                            <input type="text" class="form-control form__input-tag form__input-tag--date datepicker" id="date-from" name="date-from">
                            <input type="text" class="form-control form__input-tag form__input-tag--time timepicker" id="time-from" name="time-from">
                        </div>
                    </div>
                    <div class="form-group form__input form__input--to">
                        <label for="date-to" class="form__input-label">To</label>
                        <div class="d-flex">
                            <input type="text" class="form-control form__input-tag form__input-tag--date datepicker" id="date-to" name="date-to">
                            <input type="text" class="form-control form__input-tag form__input-tag--time timepicker" id="time-to" name="time-to">
                        </div>
                    </div>
                    <div class="form-group form__input">
                        <label for="date-from" class="form__input-label">Type Meeting</label>
                        <select class="form-control form__input-tag" id="date-from" name="date-from">
                            <option>Offline</option>
                            <option>Online</option>
                        </select>
                    </div>
                    <div class="form-group form__input">
                        <label for="description" class="form__input-label">Description</label>
                        <textarea class="form-control form__input-tag form__input-tag--textarea" id="description" name="description">Add Description...</textarea>
                    </div>
                    <div class="footer footer--pb-50">
                        Stay Connected with TAG | <a href="https://tagid.ai/#pricing">Create yours!</a>
                    </div>
                    <div class="d-flex">
                        <button id="cancel-meeting" class="btn button button--grey button--grid-2" type="button">Cancel</button>
                        <button class="btn button button--grid-2" type="submit">Schedule</button>
                    </div>
                </form>
            </div>

				<div>
            	@if (session('status'))
               	<div class="alert alert-danger">
                    {{ session('status') }}
               	</div>
               @endif
            </div>

            <div class="edit-profile">
                
                <!--<form method="post" action="{{ route('saveedit', [$data->username]) }}" enctype="multipart/form-data">-->
					 <form method="post" action="{{ route('saveedit', [$data->username]) }}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="bgedit" id="background" style="background-image: url('{{ asset('dbc/assets/images/'.$data->background) }}')">
			<div class="d-flex">
			    <font color = white style="font-size:7pt;margin-left:20px;margin-bottom:-35px;font-weight:bold">Logo size : 125 x 60 </font>
                        </div>
                        <div class="logo logo--black">
                            <img src="{{ asset('dbc/assets/images/'.$data->company_logo) }}" class="img-fluid" id="company_logo" width="125px" height="60px" />     
                        </div>
                        <div class="form__profile-edit rounded-circle">
                            <img src="{{ asset('dbc/assets/images/icon-edit.svg') }}" class="img-fluid"/>
                            <input type="file" name="company_logo" accept='image/*' id="change_company_logo"/>
                        </div>
                        <div class="form__profile">
			    <div class="d-flex">
			    	<font color = white style="font-size:7pt;margin-left:8px;margin-bottom:-20px;font-weight:bold">Profile size : 300 x 300 </font>
                       	    </div>
                            <div class="form__profile-image rounded-circle">
                                <img src="{{ asset('dbc/assets/images/'.$data->logo) }}" class="img-fluid" id="profile" />
                            </div>
                            <div class="form__profile-edit rounded-circle">
                                <img src="{{ asset('dbc/assets/images/icon-edit.svg') }}" class="img-fluid" />
										  <input type="file" name="profile" accept='image/*' id="change_profile"/>
                            </div>
									 <!--<div class="form__profile-edit rounded-circle">
                                <a href = {{ route('photoprofile', [$data->username]) }}><img src="{{ asset('dbc/assets/images/icon-edit.svg') }}" class="img-fluid" /></a>
                                <input type="file" name="profile" id="change_profile" accept=".jpg, .png, .jpeg, .gif, .bmp" />
                            </div>-->
                        </div> 
                        <div class="form__profile-edit rounded-circle" style="margin-top:-15px; margin-left:80%">
                            <img src="{{ asset('dbc/assets/images/icon-edit.svg') }}" class="img-fluid" />
                            <input type="file" name="bg_image" accept='image/*' id="change_background"/>
                        </div>
                        <div class="col-sm-12" style="text-align:right;">
			    <font color = white style="font-size:7pt;margin-bottom:20px;font-weight:bold">Background Size : 250 x 450 </font>
                       	</div>

                        <div class="form__button" style="margin-top:30px">
                            <!--<button class="btn button button--grid-1" onclick="location.href='{{ url('jadwal_setmeeting/'.$data->username) }}'">Manage My Meeting</button>-->
                            <!--<button type=button class="btn button button--grid-1" onclick="window.location='{{ route('schedule', [$data->username]) }}'">Manage My Meeting</button>-->
									 <button type=button class="btn button button--grid-1" onclick="window.location='{{ route('schedule', [$data->username]) }}'">Manage My Meeting</button>
                        </div>
                        
  										<div class="card-deck">
    										<div class="card shadow-sm bg-white rounded">
												<div class="card-header text-white" style="background-color:#10C582; font-family: Poppins;font-size: 12px;" align=center>Join Date</div>
      										<div class="card-body">
        											<p class="card-text" align=center>{{$data->transaction_date}}</p>
      										</div>
    										</div>
											<div class="card shadow-sm bg-white rounded">
												<div class="card-header text-white" style="background-color:#10C582; font-family: Poppins;font-size: 12px;" align=center>Active</div>
      										<div class="card-body">
        											<p class="card-text" align=center>{{$data->active_day}} Days</p>
      										</div>
    										</div>
											<div class="card shadow-sm bg-white rounded">
												<div class="card-header text-white" style="background-color:#10C582; font-family: Poppins;font-size: 12px;" align=center>Expired Date</div>
      										<div class="card-body">
        											<p class="card-text" align=center>{{$data->expired}}</p>
      										</div>
    										</div>	
										</div>
								
								<div class="edit-profile__title">
                            Personal Edit
                        </div>

                        <div class="form-group form__input">
                            <label for="name" class="form__input-label">Name</label>
                            <input type="text" class="form-control form__input-tag" id="name" name="name" placeholder="Name" value = "{{ $data->nama }} ">
                        </div>
                        <div class="form-group form__input">
                            <label for="director" class="form__input-label">Title</label>
                            <input type="text" class="form-control form__input-tag" id="director" name="title" placeholder="Title" value = "{{ $data->profile }} ">
                        </div>
                        <div class="form-group form__input">
                            <label for="email" class="form__input-label">Email</label>
                            <input type="email" class="form-control form__input-tag" id="email" name="email" placeholder="Email" value = "{{ $data->email }} ">
                        </div>
                        <div class="form-group form__input">
                            <label for="phone" class="form__input-label">Phone</label>
                            <input type="text" class="form-control form__input-tag" id="phone" name="phone" placeholder="Phone" value = "{{ $data->phone }} " onkeypress='validate(event)'>
                        </div>
                        <div class="form-group form__input">
                            <label for="link_room" class="form__input-label">Link Room</label>
                            <input type="text" class="form-control form__input-tag" id="link_room" name="link_room" placeholder="Link Room" value = {{ $data->link_room }}>
                        </div>
                        <!--@password = Crypt::decrypt($data->password)
                        <div class="form-group form__input">
                            <label for="password" class="form__input-label">Password</label>
                            <input type="password" class="form-control form__input-tag" id="password" name="password" placeholder="Password" value = {{ @password }}>
                        </div>-->
                        <!--<div class="form-group form__input">
                            <label for="description" class="form__input-label">Production Office</label>
                            <textarea class="form-control form__input-tag form__input-tag--textarea" id="company_production_office" name="company_production_office" placeholder="Production Office Address">{{ $data->company_production_office }}</textarea>
                        </div>-->
                        <div class="form-group form__input">
                            <label for="description" class="form__input-label">Operational Office</label>
                            <textarea class="form-control form__input-tag form__input-tag--textarea" id="company_operational_office" name="company_operational_office" placeholder="Operational Office Address">{{ $data->company_operational_office }}</textarea>
                        </div>
                        <div class="edit-profile__title">
                            Detail Company
                        </div>
                        <div class="form-group form__input">
                            <label for="company_name" class="form__input-label">Company Name</label>
									 @if($data->company_name != NULL)
                            	<input type="text" class="form-control form__input-tag" id="company_name" name="company_name" value = "{{ $data->company_name }} ">
									 @else
										<input type="text" class="form-control form__input-tag" id="company_name" name="company_name" placeholder="Company Name">	
								    @endif
                        </div>
                        <div class="form-group form__input">
                            <label for="company_description" class="form__input-label">Company Description</label>
                            <textarea class="form-control form__input-tag form__input-tag--textarea" id="company_description" name="company_description" placeholder="Company Description">{{ $data->company_description }}</textarea>
                        </div>
                        <div class="form-group form__input">
                            <label for="company_website" class="form__input-label">Company Website</label>
									 @if($data->company_contact_website != NULL)	
                            	<input type="text" class="form-control form__input-tag" id="company_contact_website" name="company_contact_website" placeholder="https://company_website.com" value = "{{ $data->company_contact_website }} ">
									 @else
										<input type="text" class="form-control form__input-tag" id="company_contact_website" name="company_contact_website" placeholder="https://company_website.com">

                            @endif
                        </div>
                        <div class="form-group form__input">
                            <label for="company_contact_phone" class="form__input-label">Company Phone Number</label>
									 @if($data->company_contact_phone != NULL)	
                            	<input type="text" class="form-control form__input-tag" id="company_contact_phone" name="company_contact_phone"value = "{{ $data->company_contact_phone }} " placeholder="6221xxxxxxxxx" onkeypress='validate(event)'>
									 @else
										<input type="text" class="form-control form__input-tag" id="company_contact_phone" name="company_contact_phone" placeholder="6221xxxxxxxxx" onkeypress='validatecompanyphone(event)'>
                            @endif
                        </div>
                        <div class="form-group form__input">
                            <label for="company_contact_mobile" class="form__input-label">Company Mobile Number</label>
									 @if($data->company_contact_mobile != NULL)
                            	<input type="text" class="form-control form__input-tag" id="company_contact_mobile" name="company_contact_mobile" placeholder="6281xxxxxxxxxxx" value = "{{ $data->company_contact_mobile }} " onkeypress='validate(event)'>
									 @else
										<input type="text" class="form-control form__input-tag" id="company_contact_mobile" name="company_contact_mobile" placeholder="6281xxxxxxxxxxx" onkeypress='validatecompanymobile(event)'>
                            @endif
                        </div>
                        <div class="edit-profile__title">
                            Social
                        </div>
                        <div class="form-group form__input">
                            <label for="instagram" class="form__input-label">Instagram</label>
                            <input type="text" class="form-control form__input-tag" id="instagram" name="instagram" placeholder="Insert your instagram username without @" value = {{ $data->instagram }}>
                        </div>
                        <div class="form-group form__input">
                            <label for="linkedIn" class="form__input-label">LinkedIn</label>
                            <input type="text" class="form-control form__input-tag" id="linkedIn" name="linkedIn" placeholder="Insert your username linkedlin" value = {{ $data->linkedin }}>
                        </div>
                        <div class="form-group form__input">
                            <label for="line" class="form__input-label">Line</label>
                            <input type="text" class="form-control form__input-tag" id="line" name="line" placeholder="Insert your line id" value = {{ $data->line }}>
                        </div>
                        <div class="form-group form__input">
                            <label for="instagram" class="form__input-label">Youtube</label>
                            <input type="text" class="form-control form__input-tag" id="youtube" name="youtube" placeholder="https://youtube.com/watch?v=........" value = {{ $data->youtube }}>
                        </div>
                        <div class="edit-profile__title">
                            Url
                        </div>
                        {{-- <div class="form-group form__input">
                            <label for="instagram" class="form__input-label">Enter Room</label>
                            <input type="text" class="form-control form__input-tag" id="url_room" name="url_room" placeholder="Enter Room" value = {{ $data->enter_room }}>
                        </div> --}}
                        <div class="form-group form__input">
                            <label for="linkedIn" class="form__input-label">Commerce</label>
                            <input type="text" class="form-control form__input-tag" id="url_commerce" name="url_commerce" placeholder="https://your commerce link" value = {{ $data->commerce }}>
                        </div>
                        <div class="form__button">
                            <button class="btn button button--grid-1" type="submit">Publish</button>
                        </div>
                    <div>
                </form>
            </div>
        </div>
        @endforeach

        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="{{ asset('dbc/assets/js/bootstrap.min.js') }}"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
        <script src="{{ asset('dbc/assets/js/bootstrap-datetimepicker.min.js') }}"></script>
        <script type="text/javascript">
                        
            $(window).on('load', function () {
                $('.profile__image').animate({
                    'marginTop': '16px'
                }, 1500 );

                updateBodyBar();
            });

            $(window).resize(function(event) {
                updateBackground();
            });
            
            $(function(){
              $('#change_company_logo').change(function(){
					 var input = this;
                var url = $(this).val();
                var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
                	if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {  
                    var reader = new FileReader();
                    reader.onload = function (e) {							
                       $('#company_logo').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                  }
                  else {
                    $('#img').attr('src', 'dbc/assets/images/no_preview.png');
                  }
              });
            });
            
            $(function(){
              $('#change_profile').change(function(){
                var input = this;
                var url = $(this).val();
                var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
                if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) 
                 {
                    var reader = new FileReader();
            
                    reader.onload = function (e) {
                       $('#profile').attr('src', e.target.result);
                    }
                   reader.readAsDataURL(input.files[0]);
                }
                else
                {
                  $('#img').attr('src', '/assets/no_preview.png');
                }
              });
            
            });
            
            $(function(){
              $('#change_background').change(function(){
                var input = this;
                var url = $(this).val();                var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
                if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) 
                 {
                    var reader = new FileReader();
            
                    reader.onload = function (e) {
                       //$('#background').css('background-image', url("+e.target.result+")");
		       document.getElementById("background").style.backgroundImage = "url('"+e.target.result+"')";	
                    }
                   reader.readAsDataURL(input.files[0]);
                }
                else
                {
                  $('#background').attr('src', '/assets/no_preview.png');
                }
              });
            
            });

            $(document).ready(function() {
                var height = $(window).height();
                var profile_h = $('.profile').height();
                var margin_top = (height - profile_h - 92) / 2;

                $('.profile__image').css('margin-top',margin_top);

                updateBackground();

                $('#schedule-meeting').click(function(){
                    $('.background').removeClass('animate__fadeInLeft').addClass('animate__animated').addClass('animate__fadeOutLeft'), setTimeout(function () {
                        $('.background').addClass('d-none');
                    }, 800);
                    
                    $('.meeting').removeClass('animate__fadeOutLeft').removeClass('d-none').addClass('animate__animated').addClass('animate__fadeInLeft'), setTimeout(function () {
                        updateBackground();
                    }, 800);
                });

                $('#cancel-meeting').click(function(){
                    $('.meeting').removeClass('animate__fadeInLeft').addClass('animate__fadeOutLeft'), setTimeout(function () {
                        $('.meeting').addClass('d-none');
                    }, 800);

                    $('.background').removeClass('animate__fadeOutLeft').removeClass('d-none').addClass('animate__fadeInLeft'), setTimeout(function () {
                        updateBackground();
                    }, 800);
                });

                var company_h = 0;

                $('.information__top-bar').click(function(){
                    let action = $(this).data('action');
                    let ch = $('.information__company').height();
                    if(ch!=0){
                        company_h = ch;
                    }

                    if(action=="top"){
                        $('.profile__image').animate({
                            'marginTop': '250px'
                        }, 1500 );

                        $('.profile__name').animate({
                            'marginTop': '-330px',
                            'marginBottom': '330px',
                        }, 1500 );

                        $('.profile__name-size').removeClass('profile__name-size--small').addClass('profile__name-size--big');

                        $('.information__company').animate({
                            'height': '0px'
                        }, 1500 );

                        $('.information__about-me-title').animate({
                            'marginTop': '0px'
                        }, 1500 );

                        var height = $(window).height();
                        var bodybar_h = height - (170);

                        $('.information__body-bar').removeClass('d-none').animate({
                            'height': bodybar_h+'px'
                        }, 1500, function() {
                            $('.information__body-bar').css('overflow-y','scroll');
                        });

                        $(this).data('action','bottom');
                    }else if(action=="bottom"){
                        $('.profile__image').animate({
                            'marginTop': '16px'
                        }, 1500 );

                        $('.profile__name').animate({
                            'marginTop': '12px',
                            'marginBottom': '0px',
                        }, 1500 );

                        $('.profile__name-size').removeClass('profile__name-size--big').addClass('profile__name-size--small');

                        $('.information__company').animate({
                            'height': company_h+'px'
                        }, 1500 );

                        $('.information__about-me-title').animate({
                            'marginTop': '24px'
                        }, 1500 );
                        
                        updateBodyBar();

                        $(this).data('action','top');
                    }else{
                        $('.information__qr-code').animate({
                            'opacity': '0'
                        }, 800, function() {
                            $('.information__qr-code').addClass('d-none');
                            $('.information__body-bar-content').removeClass('d-none');
                        });

                        $('.information__body-bar-content').animate({
                                'opacity': '1'
                        }, 2500 );

                        $(this).data('action','top');
                    }
                });

                $('#share-button').click(function(){
                    $('.information__body-bar-content').animate({
                            'opacity': '0'
                    }, 800, function() {
                        $('.information__body-bar-content').addClass('d-none');
                        $('.information__qr-code').removeClass('d-none');
                    });

                    $('.information__qr-code').animate({
                        'opacity': '1'
                    }, 2500 );

                    $('.information__top-bar').data('action','back');
                });

                $('.information__button-bottom-grey').click(function(){
                    $('.information__button-bottom-grey').toggleClass('information__button-bottom-grey--active')
                    
                    if($(this).data('active')==1){
                        $('.information__button-bottom-popup').css('height','110px').animate({
                            'opacity': '1'
                        }, 1500 );
                        $(this).data('active','2');
                    }else{
                        $('.information__button-bottom-popup').animate({
                            'opacity': '0'
                        }, 1500, function() {
                            $('.information__button-bottom-popup').css('height','0px');
                        });
                        $(this).data('active','1');
                    }
                });

                $('.datepicker').datetimepicker({
                    useCurrent: false,
                    format: 'L'
                });
                $('.timepicker').datetimepicker({
                    useCurrent: false,
                    format: 'LT'
                });

                $("#date-from").on("dp.change", function (e) {
                    $('#time-from').data("DateTimePicker").minDate(e.date);
                    $('#date-to').data("DateTimePicker").minDate(e.date);
                    $('#time-to').data("DateTimePicker").minDate(e.date);
                });
                $("#time-from").on("dp.change", function (e) {
                    $('#date-from').data("DateTimePicker").minDate(e.date);
                    $('#date-to').data("DateTimePicker").minDate(e.date);
                    $('#time-to').data("DateTimePicker").minDate(e.date);
                });
                $("#date-to").on("dp.change", function (e) {
                    $('#date-from').data("DateTimePicker").maxDate(e.date);
                    $('#time-from').data("DateTimePicker").maxDate(e.date);
                });
                $("#time-to").on("dp.change", function (e) {
                    $('#date-from').data("DateTimePicker").maxDate(e.date);
                    $('#time-from').data("DateTimePicker").maxDate(e.date);
                });
            });

	   		function validate(evt) {
				var theEvent = evt || window.event;
      		    var phone = document.getElementById("phone");

				// Handle paste
                if (theEvent.type === 'paste') {
                    key = event.clipboardData.getData('text/plain');
                } else {
                    // Handle key press
                    var key = theEvent.keyCode || theEvent.which;
                    key = String.fromCharCode(key);
                }
                var regex = /[0-9]|\./;
                if( (!regex.test(key)) || (phone.value.length > 20) ) {
                    theEvent.returnValue = false;
                    if(theEvent.preventDefault) theEvent.preventDefault();
                }
            };

				function validatecompanyphone(evt) {
				var theEvent = evt || window.event;
      		var phone = document.getElementById("company_contact_mobile");

				// Handle paste
                if (theEvent.type === 'paste') {
                    key = event.clipboardData.getData('text/plain');
                } else {
                    // Handle key press
                    var key = theEvent.keyCode || theEvent.which;
                    key = String.fromCharCode(key);
                }
                var regex = /[0-9]|\./;
                if( (!regex.test(key)) || (phone.value.length > 20) ) {
                    theEvent.returnValue = false;
                    if(theEvent.preventDefault) theEvent.preventDefault();
                }
            };

				function validatecompanymobile(evt) {
				var theEvent = evt || window.event;
      		var phone = document.getElementById("company_contact_mobile");

				// Handle paste
                if (theEvent.type === 'paste') {
                    key = event.clipboardData.getData('text/plain');
                } else {
                    // Handle key press
                    var key = theEvent.keyCode || theEvent.which;
                    key = String.fromCharCode(key);
                }
                var regex = /[0-9]|\./;
                if( (!regex.test(key)) || (phone.value.length > 20) ) {
                    theEvent.returnValue = false;
                    if(theEvent.preventDefault) theEvent.preventDefault();
                }
            };



            function updateBackground(){
                var container_w = $('.container-mobile').width();
                $('.information').css('width', container_w);
                $('.information__button-bottom').css('width', container_w);
                $('.meeting').css('width', container_w);

                var height = $(window).height();
                var bg_h = $('.background').height();
                var meeting_h = $('.meeting').height();
                $('.background').css('height', height);
                $('.meeting').css('height', height);
            }

            function updateBodyBar(){
                var height = $(window).height();
                var logo_h = 55;
                var profile_h = 266;
                var topbar_h = $('.information__top-bar').height();

                var bodybar_h = height - (logo_h + profile_h + topbar_h + 110);

                $('.information__body-bar').removeClass('d-none').animate({
                    'height': bodybar_h+'px'
                }, 1500 , function() {
                    $('.information__body-bar').css('overflow-y','hidden');
                });
                console.log(bodybar_h);

                if(bodybar_h>100){
                    $('.information__button-bottom').addClass('animate__animated').addClass('animate__delay-1s').addClass('animate__fadeInUp');
                }else{
                    $('.information__button-bottom').addClass('d-none');
                }
            }
        </script>
    </body>
</html>