<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailNotify;
use App\Mail\MailSetMeeting;
use App\Mail\MailStatusMeeting;
use Carbon\Carbon;
use File;
use Calendar;


class PenggunaController extends Controller
{
    public function editPengguna($username){
        //$members = DB::select('select a.*, b.* from user a left join member b on b.user_id = a.id where b.client_id = 1 and a.username = ? ' , array($username));
			$members = DB::select('select b.member_name as nama, b.member_email as email, member_phone as phone, a.username, 
                	       			b.profil as profile, b.background, b.company_logo, b.logo,
                               	c.transaction_date, c.expired, c.expired-now() as activeday,
                                 b.company_name, b.company_description, company_contact_website, company_contact_phone, company_contact_mobile,
                               	b.instagram, b.linkedin, b.line, b.youtube, b.link_room, b.commerce,             			
                                 b.company_production_office, b.company_operational_office
        	               			from user a 
                	       			left join member b on b.user_id = a.id 
                                 left join transaction c on c.member_id = b.id
                	       			where b.client_id = 1 and a.username = ? ' , array($username));
        return view('Pengguna.edit_pengguna', ['member' => $members]);
    }
    
    public function login(){
        //$member = DB::select('select a.*, b.company_logo from user a left join member b on b.user_id = a.id where b.client_id = 1 and a.username = ? ' , array($username));
        //return view('Pengguna.pengguna', ['member' => $member]);			
		  		return view('Pengguna.pengguna');
    }  

    public function forgotpassword(){
		  //$member = DB::select('select a.* from user a left join member b on b.user_id = a.id where b.client_id = 1 and a.username = ? ' , array($username));	
    	  //return view('Pengguna.forgot_email',['member' => $member]);
		  return view('Pengguna.forgot_email');
    }
    
    public function resetpassword(Request $request){
        $members = DB::table('user')->select('user.username', 'member.member_email', 'member.member_name')->join('member','member.user_id','user.id')->where('client_id',1)->where('user.email',$request->email)->first();
		  if(DB::table('user')->select('user.username', 'member.member_email', 'member.member_name')->join('member','member.user_id','user.id')->where('client_id',1)->where('user.email',$request->email)->count() > 0){
        		if($members->member_email == $request->email){
					$token = uniqid();
					DB::table('user')->where('email',$members->member_email)->update([
               	'reset_token' => $token
            	]);
				
					$arr = [ 
						'token' => $token, 
						'username' => $members->username, 
					];
            	Mail::to($members->member_email)->send(new MailNotify($token, $members->username, $members->member_name));
					$title = "Reset Password";
            	$message = "The link for resetting the password has been sent to the email, please check your email.";	
					$members = DB::table('user')->select('user.username', 'member.member_email', 'member.member_name')->join('member','member.user_id','user.id')->where('client_id',1)->where('user.username',$members->username)->first();
					$link = "https://tagid.ai/login";
					$name = $members->member_name;
					$name_button = "Login Tagid";
			   	return view('Pengguna.message', ['title' => $title, 'message' => $message, 'username' => $members->username, 'link' => $link, 'member_name' => $name, 'name_button' => $name_button ]);
					//$member = DB::select('select a.* from user a left join member b on b.user_id = a.id where b.client_id = 1 and a.username = ? ' , array($members->username));
            	//return view('Pengguna.pengguna', ['member' => $member]);
        		}
        		else{
					return redirect()->back()->with('status', 'Your email is not registered, please check your email');
        		}
			}
			else {
				return redirect()->back()->with('status', 'Your email is not registered, please check your email');
         }
    }

	 public function new_password(Request $request, $username){
	    $members = DB::table('user')->select('user.username', 'user.email', 'user.reset_token')->where('user.username',$username)->first();
		 //return json_encode($members);
		 if($members->reset_token == $request->token){
			  $member = DB::select('select a.* from user a left join member b on b.user_id = a.id where b.client_id = 1 and a.username = ? ' , array($members->username));
           return view('Pengguna.new_password', ['member' => $member]);
		 }
       else{				
			  $username = $members->username; 	
			  $title = "Reset Password";	
           $message = "Your Token is Wrong / Expired, please click link Forgot Password";	
			  $link = "https://tagid.ai/forgot/".$username;
			  $members = DB::table('user')->select('user.username', 'member.member_email', 'member.member_name')->join('member','member.user_id','user.id')->where('client_id',1)->where('user.username',$username)->first();
			  $name = $members->member_name;
			  $name_button = "Reset Login";	
			  return view('Pengguna.message', ['title' => $title, 'message' => $message, 'username' => $username, 'link' => $link, 'member_name' => $name, 'name_button' => $name_button]);

       }
	 }

	 public function post_password(Request $request, $username){
	    $members = DB::table('user')->select('user.username', 'member.member_email', 'user.reset_token')->join('member','member.user_id','user.id')->where('client_id',1)->where('user.username',$username)->first();
		 if($request->password == $request->confirm){ 	
			  DB::table('user')->where('username',$members->username)->update([
                'password' => Hash::make($request->password)
           ]);
           return redirect('/login');
		 }
       else{		
			  return redirect()->back()->with('status', 'Confirm password is not the same, please retype again');
       }
	 }

   function postlogin(Request $request){
			if(DB::table('user')->select('user.username', 'member.member_email', 'member.member_name')->join('member','member.user_id','user.id')->where('client_id',1)->where('user.email',$request->email)->where('user.status_activation',"active")->count() > 0){
				$member = DB::table('user')->select('*')->where('email',$request->email)->first();
   	   	if(Hash::check($request->password, $member->password)){
            	$request->session()->put('user_id',$member->username);
					return redirect('https://'.$member->username.'.tagid.ai/detail');
	   	 	}
       		else{
            	return redirect()->back()->with('status', 'Wrong Password!');
       		}
			}
			else {
				return redirect()->back()->with('status', 'Email not registered in our database');
			}
   }

	function detail($username){
		$members = DB::select('select b.member_name as nama, b.member_email as email, member_phone as phone, a.username, 
                	       			b.profil as profile, b.background, b.company_logo, b.logo,
                               	date_format(c.transaction_date,"%d-%m-%Y") as transaction_date, date_format(c.expired,"%d-%m-%Y") as expired, DATEDIFF(c.expired, CURRENT_DATE) as active_day,
                                 b.company_name, b.company_description, company_contact_website, company_contact_phone, company_contact_mobile,
                               	b.instagram, b.linkedin, b.line, b.youtube, b.link_room, b.commerce,             			
                                 b.company_production_office, b.company_operational_office
        	               			from user a 
                	       			left join member b on b.user_id = a.id 
                                 left join transaction c on c.member_id = b.id
                	       			where b.client_id = 1 and a.username = ? ' , array($username));
		return view('Pengguna.edit_pengguna', ['member'=>$members]);
	}

	public function loginuser(){
		return redirect('https://tagid.ai/login');
	}
    
    public function logout(Request $request, $username){
        if($request->session()->has('username')){
            $request->session()->forget('username');
        }
        $members = DB::select('select a.* from user a left join member b on b.user_id = a.id where b.client_id = 1 and a.username = ? ' , array($username));
        return view('Pengguna.home_pengguna',['members'=>$member]);
    }

	 public function photoProfile($username){
        $members = DB::select('select a.* from user a left join member b on b.user_id = a.id where b.client_id = 1 and a.username = ? ' , array($username));
        return view('Pengguna.photoprofile', ['member' => $members]);
    }

	 public function uploadProfile(Request $request, $username)
    {
        $folderPath = public_path('public/dbc/assets/images/');

        $image_parts = explode(";base64,", $request->image);
        $image_type_aux = explode("public/dbc/assets/images/", $image_parts[0]);
        $image_type = $image_type_aux[1];
        $image_base64 = base64_decode($image_parts[1]);
        $file = $folderPath . "profile_" . $username . '.png';

        file_put_contents($file, $image_base64);

        return response()->json(['success'=>'success']);
    }
    
    function saveedit(Request $request, $username){
	     $members = DB::table('user')->select('user.id')->join('member','member.user_id','user.id')->where('client_id',1)->where('user.username',$request->username)->first();
        //$members = DB::select('select a.* from user a left join member b on b.user_id = a.id where b.client_id = 1 and a.username = ? ' , array($username));
        if(!empty($_FILES["company_logo"]["tmp_name"])){
            $destinationPath    = 'public/dbc/assets/images';
            $file               = $request->file('company_logo');
            $nama_file          = "logo_".$members->id;
            $extension          = $file->getClientOriginalExtension();
	         DB::table('member')->where('user_id',$members->id)->update([
   	         'company_logo' => "logo_".$members->id.".".$extension
      	   ]);
				$file->move($destinationPath,$nama_file.'.'.$extension);
        }
        
        if(!empty($_FILES["profile"]["tmp_name"])){
            $destinationPath    = 'public/dbc/assets/images';
            $file               = $request->file('profile');
            $nama_file          = $members->id;
            $extension          = $file->getClientOriginalExtension();
            DB::table('member')->where('user_id',$members->id)->update([
                'logo' => $members->id.".".$extension
            ]);
            $file->move($destinationPath,$nama_file.'.'.$extension);
        }

        if(!empty($_FILES["bg_image"]["tmp_name"])){
            $destinationPath    = 'public/dbc/assets/images';
            $file               = $request->file('bg_image');
            $nama_file          = "back_".$members->id;
            $extension          = $file->getClientOriginalExtension();
            DB::table('member')->where('user_id',$members->id)->update([
                'background' => "back_".$members->id.".".$extension
            ]);
            $file->move($destinationPath,$nama_file.'.'.$extension);
        }

        DB::table('member')
        ->where('user_id',$members->id)
        ->update([
    		'member_name'                   => $request->name,
    		'profil'                        => $request->title,
            	'member_email'                  => $request->email,
            	'member_country'                => $request->country_code,
            	'member_phone'                  => $request->phone,
            	'password'                      => $request->password,
    		'company_name'                  => $request->company_name,
    		'company_description'           => $request->company_description,
    		'company_contact_website'       => $request->company_contact_website,
    		'company_contact_phone'         => $request->company_contact_phone,
    		'company_contact_mobile'        => $request->company_contact_mobile,
            	'link_room'                     => $request->link_room,
            	'company_operational_office'    => $request->company_operational_office,
    		'instagram'                     => $request->instagram,
    		'linkedin'                      => $request->linkedIn,
            	'line'                          => $request->line,
            	'youtube'                       => $request->youtube,
    		'enter_room'                    => $request->url_room,
    		'commerce'                      => $request->url_commerce
	    ]);
	    
	    if($request->session()->has('username')){
            $request->session()->forget('username');
        }
        
        // $member = DB::select('select * from member where username = ?', array($request->username));
        // return view('Pengguna.home_pengguna', ['members' => $member]);
		  $member = DB::select('select b.member_name as nama, b.member_email as email, member_phone as phone, a.username, 
                                     b.profil as profile, b.background, b.company_logo, b.logo,
                                     b.company_name, b.company_description, company_contact_website, company_contact_phone, 																 company_contact_mobile,
                                     b.instagram, b.linkedin, b.line, b.youtube, c.phonecode as country_code,
                                     b.company_production_office, b.company_operational_office
                                     from user a 
                                     left join member b on b.user_id = a.id 
				     left join countries c on c.id = b.member_country
                                     where b.client_id = 1 and a.username = ? ' , array($username));
		  return view('Pengguna.home_pengguna',['members' => $member]);
        //return redirect()->action(
        //    [PenggunaController::class, 'tampilPengguna'], ['username' => $username]
        //);
        
    }

    public function editCompAdd(Request $request, $username){
		  $members = DB::select('select a.*, b.* from user a left join member b on b.user_id = a.id where b.client_id = 1 and a.username = ? ' , array($username));
        DB::table('member')->where('user_id', $members->user_id)->update([
            'company_production_office'     => $request->company_production_office,
            'company_operational_office'    => $request->company_operational_office,
        ]);
        return redirect()->action(
            [PenggunaController::class, 'tampilPengguna'], ['username' => $username]
        );
    }

    public function tampilPengguna($username){
        //$member = DB::table('member')->where('username',$username)->first();
        $member = DB::select('select b.member_name as nama, b.member_email as email, member_phone as phone, a.username, 
                                     b.profil as profile, b.background, b.company_logo, b.logo,
                                     b.company_name, b.company_description, company_contact_website, company_contact_phone, company_contact_mobile,
                                     b.instagram, b.linkedin, b.line, b.youtube, c.phonecode as country_code,
                                     b.company_production_office, b.company_operational_office
                                     from user a 
                                     left join member b on b.user_id = a.id 
				     left join countries c on c.id = b.member_country
                                     where b.client_id = 1 and a.username = ? ' , array($username));
        //return $member;
        if (session()->has('username') != $username){
            session()->forget('username');
        }
        return view('Pengguna.home_pengguna',['members'=>$member]);
    }

    public function showPhoto($username){
        $member = DB::select('select b.member_name as nama, b.member_email as email, member_phone as phone, a.username, 
                                     b.profil as profile, b.background, b.company_logo, b.logo,
                                     b.company_name, b.company_description, company_contact_website, company_contact_phone, company_contact_mobile,
                                     b.instagram, b.linkedin, b.line, b.youtube, c.phonecode as country_code, 
                                     b.company_production_office, b.company_operational_office
                                     from user a 
                                     left join member b on b.user_id = a.id 
				     left join countries c on c.id = b.member_country
                                     where b.client_id = 1 and a.username = ? ' , array($username));        
	return view('Pengguna.photo',['members'=>$member]);
    }

    public function enterRoom($username){
	$member = DB::select('select b.member_name as nama, b.member_email as email, member_phone as phone, a.username, 
                                     b.profil as profile, b.background, b.company_logo, b.logo,
                                     b.company_name, b.company_description, company_contact_website, company_contact_phone, company_contact_mobile,
                                     b.instagram, b.linkedin, b.line, b.youtube, c.phonecode as country_code, b.link_room,
                                     b.company_production_office, b.company_operational_office
                                     from user a 
                                     left join member b on b.user_id = a.id 
				     left join countries c on c.id = b.member_country
                                     where b.client_id = 1 and a.username = ? ' , array($username));        
        return view('Pengguna.enterroom_pengguna',['members'=>$member]);
    }

    public function commerce($username){
        $member = DB::select('select b.member_name as nama, b.member_email as email, member_phone as phone, a.username, 
                                     b.profil as profile, b.background, b.company_logo, b.logo,
                                     b.company_name, b.company_description, company_contact_website, company_contact_phone, company_contact_mobile,
                                     b.instagram, b.linkedin, b.line, b.youtube, c.phonecode as country_code, b.link_room, b.commerce,
                                     b.company_production_office, b.company_operational_office
                                     from user a 
                                     left join member b on b.user_id = a.id 
				     left join countries c on c.id = b.member_country
                                     where b.client_id = 1 and a.username = ? ' , array($username));
       return view('Pengguna.commerce_pengguna',['members'=>$member]);
    }

    public function tambahSchedule(){
        return view('Pengguna.home_pengguna');
    }
    
    public function tambahScheduleProcess(Request $request){
        DB::table('schedule')->insert([
            'topic'             => $request->topic,
            'date_from'         => $request->date_from,
            'date_to'           => $request->date_to,
            'type_meeting'      => $request->type_meeting,
            'description'       => $request->description
        ]);
        return view('Pengguna.home_pengguna');
    }

    public function jadwalSetMeeting($username){
        $member = DB::select('select a.*, b.company_logo from user a left join member b on b.user_id = a.id where b.client_id = 1 and a.username = ? ' , array($username));
        $data = DB::select('select * from setmeeting where username = ?', array($username));

        return view('Pengguna.jadwal_setmeeting',['data'=>$data], ['member'=>$member]);
    }

    
    public function jadwalCalendar($username){
        $member = DB::select('select a.*, b.company_logo from user a left join member b on b.user_id = a.id where b.client_id = 1 and a.username = ? ' , array($username));
        $data = DB::select('select * from setmeeting where username = ?', array($username));
        return view('Pengguna.calendar_setmeeting',['data'=>$data], ['member'=>$member]);
        //return view('Pengguna.calendar_setmeeting',['data'=>$data], ['member'=>$member]);
    }

    public function detailMeeting($username, $id){
        $member = DB::select('select a.*, b.company_logo from user a left join member b on b.user_id = a.id where b.client_id = 1 and a.username = ? ' , array($username));
        $data = DB::select('select * from setmeeting where username = ? and id = ?', array($username,$id) );
        return view('Pengguna.detailmeeting',['member'=>$member],['data'=>$data]);
    }

    public function SetStatusMeeting($username, $id){
        $member = DB::select('select a.*, b.company_logo from user a left join member b on b.user_id = a.id where b.client_id = 1 and a.username = ? ' , array($username));
        $data = DB::select('select * from setmeeting where username = ? and id = ?', array($username,$id) );
        return view('Pengguna.statusmeeting',['member'=>$member],['data'=>$data]);
    }

    public function SaveStatusMeeting(Request $request, $username, $id){
        if($request->status == "ON REVIEW"){
            DB::table('setmeeting')->where(['id'=>$id,'username'=>$username])->update([
                'status'=> null
            ]);  
        }
        else{
            DB::table('setmeeting')->where(['id'=>$id,'username'=>$username])->update([
                'status'=> $request->status
            ]);
        }

		  
		  $meeting = DB::table('setmeeting')->select('*')->where('username',$request->username)->where('id',$id)->first();
	
		  //$message = "Request schedule meeting ". $meeting->type_meeting." with topic ". $meeting->topik ." in ". $meeting->date . " has been " . $request->status . "\n\n".$request->deskripsi;
		  $message = "Your meeting request: ". $meeting->type_meeting ." has been " . $request->status . " with topic ". $meeting->topik ." in " .$meeting->date;	

		  if((substr($meeting->phone,0,3) == "+62")||(substr($meeting->phone,0,3) == "+60")){
				$phone = $meeting->phone;
		  }
        else{
				if(substr($meeting->phone,0,1) == "0"){
					$phone = "+62".substr($meeting->phone,1);
			   }
        }	
		  //$phone = $request->phone;	  
        $phone_no = $phone;
        
        $message = preg_replace( "/(\n)/", "<ENTER>", $message );
        $message = preg_replace( "/(\r)/", "<ENTER>", $message );
        
        $phone_no = preg_replace( "/(\n)/", ",", $phone_no );
        $phone_no = preg_replace( "/(\r)/", "", $phone_no );
        
        $data = array("phone_no" => $phone_no, "key" => "a69a96a096c5c361070c35dbd7956a7225df0e0a76680ff0", "message" => $message);
        $data_string = json_encode($data);
        $ch = curl_init('http://116.203.92.59/api/send_message');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
        );
        $result = curl_exec($ch);
        //End Of Send Message to Whatsapp

		  //Send Message to Email
				Mail::to($meeting->email)->send(new MailStatusMeeting($meeting->email, $meeting->topik, $meeting->date, $message, $request->status));		
        //End of Send Message to Email	

		  //Send to card member
		  $dphone = DB::table('member')->select('member.member_phone', 'member.member_email')->join('user','user.id','member.user_id')->where('member.client_id',1)->where('user.username',$username)->first();	
		  if((substr($dphone->member_phone,0,2) == "62")||(substr($dphone->member_phone,0,2) == "60")){
				$phone = "+".$dphone->member_phone;
		  }
        else{
				if(substr($dphone->member_phone,0,1) == "0"){
					$phone = "+62".substr($dphone->member_phone,1);
			   }
        }	
		  //$phone = $request->phone;	  
        $phone_no = $phone;
        
        $message = preg_replace( "/(\n)/", "<ENTER>", $message );
        $message = preg_replace( "/(\r)/", "<ENTER>", $message );
        
        $phone_no = preg_replace( "/(\n)/", ",", $phone_no );
        $phone_no = preg_replace( "/(\r)/", "", $phone_no );
        
        $data = array("phone_no" => $phone_no, "key" => "a69a96a096c5c361070c35dbd7956a7225df0e0a76680ff0", "message" => $message);
        $data_string = json_encode($data);
        $ch = curl_init('http://116.203.92.59/api/send_message');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
        );
        $result = curl_exec($ch);
        //End Of Send Message to Whatsapp

		  //Send Message to Email
				Mail::to($meeting->email)->send(new MailStatusMeeting($dphone->member_email, $meeting->topik, $meeting->date, $message, $request->status));		
        //End of Send Message to Email

	
        $member = DB::select('select a.*, b.company_logo from user a left join member b on b.user_id = a.id where b.client_id = 1 and a.username = ? ' , array($username));
        $data = DB::select('select * from setmeeting where username = ? ', array($username) );        
	return view('Pengguna.jadwal_setmeeting',['data'=>$data], ['member'=>$member]);
    
    }

    public function setMeeting(Request $request, $username){
        if($request->session()->get('user_id')==$username){
            $data['session']  = array(
                'id'            => $request->session()->get('s_id'),
                'username'      => $request->session()->get('username'),
                'topik'         => $request->session()->get('s_topik'),
                'date'          => $request->session()->get('s_date'),
                'due'           => $request->session()->get('s_due'),
                'type_meeting'  => $request->session()->get('s_type_meeting'),
                'deskripsi'     => $request->session()->get('s_deskripsi')
            );
            $members = DB::select('select a.*, b.company_logo from user a left join member b on b.user_id = a.id where b.client_id = 1 and a.username = ? ' , array($username));
            $data = DB::select('select * from setmeeting where username = ?', array($username));
            // return view('Pengguna.jadwal_setmeeting',['data'=>$data], ['member'=>$member]);
            return view('Pengguna.set_meeting',$data, ['member'=>$members]);
        }
        else{
            /*$members = DB::select('select a.*, b.company_logo from user a left join member b on b.user_id = a.id where b.client_id = 1 and a.username = ? ' , array($username));
            $data = DB::select('select * from setmeeting where username = ?', array($username));
            return view('Pengguna.set_meeting',$data, ['member'=>$members]);*/
				return view('home.activationfailed');
        }
    }

    public function setMeetingSave(Request $request, $username)
    {
        
        // // return redirect('/loginpengguna');
        // return redirect('Pengguna.jadwal_setmeeting');
        
        //Send Message to Whatsapp
        //$phone = DB::table('member')->select('phone')->where('username',$request->username)->first();
        //$members = DB::select('select concat(c.phonecode,b.member_phone) as phone from user a left join member b on b.user_id = a.id left join countries c on c.id = b.member_country where b.client_id = 1 and a.username = ? ' , array($username));
        //$members = DB::table('user')->select('countries.phonecode', 'member.member_phone')->join('member','member.user_id','user.id')->join('countries','countries.id','member.member_country')->where('member.client_id',1)->where('user.username',$request->username)->first();
		
	     //$message = "Set Meeting ". $request->type_meeting." telah dibuat dengan topik ". $request->topik ." pada tanggal ". $request->date ." jam ".              $request->time ."\n\n".$request->deskripsi;
        
        //$phone_no = "+".$members->phonecode.$members->member_phone;


			//$d = $request->date;
			//$d =  Carbon::createFromFormat('d-m-Y H:i A', $request->date)->format('Y-m-d H:i A');
			//return $d ;

			DB::table('setmeeting')->insert([
            'username'      => $request->username,
            'topik'         => $request->topik,
            'email'         => $request->email,
            'phone'         => $request->phone,
            'date'          => Carbon::createFromFormat('d-m-Y H:i', $request->date)->format('Y-m-d H:i'),
            'due'           => Carbon::createFromFormat('d-m-Y H:i', $request->due)->format('Y-m-d H:i'),
            'type_meeting'  => $request->type_meeting,
            'deskripsi'     => $request->deskripsi
        ]);

			$message = "Request schedule meeting ". $request->type_meeting." has been made with topic ". $request->topik ." in ". $request->date ." with description ". $request->deskripsi;
		  //Send to card maker
		  if((substr($request->phone,0,3) == "+62")||(substr($request->phone,0,3) == "+60")){
				$phone = $request->phone;
		  }
        else{
				if(substr($request->phone,0,1) == "0"){
					$phone = "+62".substr($request->phone,1);
			   }
        }	
		  //$phone = $request->phone;	  
        $phone_no = $phone;
        
        $message = preg_replace( "/(\n)/", "<ENTER>", $message );
        $message = preg_replace( "/(\r)/", "<ENTER>", $message );
        
        $phone_no = preg_replace( "/(\n)/", ",", $phone_no );
        $phone_no = preg_replace( "/(\r)/", "", $phone_no );
        
        $data = array("phone_no" => $phone_no, "key" => "a69a96a096c5c361070c35dbd7956a7225df0e0a76680ff0", "message" => $message);
        $data_string = json_encode($data);
        $ch = curl_init('http://116.203.92.59/api/send_message');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
        );
        $result = curl_exec($ch);
        //End Of Send Message to Whatsapp

		  //Send Message to Email
				Mail::to($request->email)->send(new MailSetMeeting($request->email, $request->topik, $request->date , $message));		
        //End of Send Message to Email
		  //End of Send to card maker

		  //Send to card member
		  $dphone = DB::table('member')->select('member.member_phone', 'member.member_email')->join('user','user.id','member.user_id')->where('member.client_id',1)->where('user.username',$username)->first();	
		  if((substr($dphone->member_phone,0,2) == "62")||(substr($dphone->member_phone,0,2) == "60")){
				$phone = "+".$dphone->member_phone;
		  }
        else{
				if(substr($dphone->member_phone,0,1) == "0"){
					$phone = "+62".substr($dphone->member_phone,1);
			   }
        }	
		  //$phone = $request->phone;	  
        $phone_no = $phone;
        
        $message = preg_replace( "/(\n)/", "<ENTER>", $message );
        $message = preg_replace( "/(\r)/", "<ENTER>", $message );
        
        $phone_no = preg_replace( "/(\n)/", ",", $phone_no );
        $phone_no = preg_replace( "/(\r)/", "", $phone_no );
        
        $data = array("phone_no" => $phone_no, "key" => "a69a96a096c5c361070c35dbd7956a7225df0e0a76680ff0", "message" => $message);
        $data_string = json_encode($data);
        $ch = curl_init('http://116.203.92.59/api/send_message');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
        );
        $result = curl_exec($ch);
        //End Of Send Message to Whatsapp

		  //Send Message to Email
				Mail::to($dphone->member_email)->send(new MailSetMeeting($dphone->member_email, $request->topik, $request->date , $message));		
        //End of Send Message to Email
		  //End of Send to card member

        
        $member = DB::select('select b.member_name as nama, b.member_email as email, member_phone as phone, a.username, 
                                     b.profil as profile, b.background, b.company_logo, b.logo,
                                     b.company_name, b.company_description, company_contact_website, company_contact_phone, company_contact_mobile,
                                     b.instagram, b.linkedin, b.line, b.youtube, c.phonecode as country_code,
                                     b.company_production_office, b.company_operational_office
                                     from user a 
                                     left join member b on b.user_id = a.id 
				     left join countries c on c.id = b.member_country
                                     where b.client_id = 1 and a.username = ? ' , array($username));
        $data = DB::table('setmeeting')->get();
        //return view('Pengguna.set_meeting',$data, ['member'=>$members]);
		  return view('Pengguna.home_pengguna',['members'=>$member]);

        //return view('Pengguna.jadwal_setmeeting',['data'=>$data], ['member'=>$members]);
        //return redirect()->route('jadwal_setmeeting', [$username]);
        
    }

    public function EditSetMeeting(Request $request, $id){
        //$member = DB::select('select * from member where username = ?', array($request->username));
        $members = DB::select('select a.* from user a left join member b on b.user_id = a.id where b.client_id = 1 and a.username = ? ' , array($request->username));
        $setmeeting = DB::table('setmeeting')->where('id', $id)->first();
        // dd($setmeeting);
        // return $member;
        return view('Pengguna.edit_setMeeting', ['setmeeting' => $setmeeting], ['member'=>$member]);
    }

    public function editSetMeetingsave(Request $request, $username){
        $method = $request->method();
        $members = DB::select('select a.* from user a left join member b on b.user_id = a.id where b.client_id = 1 and a.username = ? ' , array($username));
        $data = DB::table('setmeeting')->get();
        if($method=="POST"){
            DB::table('setmeeting')->where('id', $request->input('id'))->update([
                'topik'         => $request->input('topik'),
                'email'         => $request->input('email'),
                'date'          => $request->input('date'),
                'due'           => $request->input('due'),
                'type_meeting'  => $request->input('type_meeting'),
                'deskripsi'     => $request->input('deskripsi'),
            ]);
            return redirect()->route('jadwal_setmeeting', [$username]);
        } else{
            return redirect()->route('jadwal_setmeeting', [$username]);
        }
    }

    public function deleteSetMeeting($id, $username){
        $data = DB::select('select * from setmeeting where username = ?', array($username));
        if($data!=NULL){
            DB::table('setmeeting')->where('id', $id)->where('username', $username)->delete();
            $members = DB::select('select a.* from user a left join member b on b.user_id = a.id where b.client_id = 1 and a.username = ? ' , array($username));
            return redirect()->route('jadwal_setmeeting', [$username]);
        }
        else{
            return redirect()->route('jadwal_setmeeting', [$username]);
        }
    }
}
