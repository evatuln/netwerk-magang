/*=========================================================================================
  File Name: sidebarItems.js
  Description: Sidebar Items list. Add / Remove menu items from here.
  Strucutre:
          url     => router path
          name    => name to display in sidebar
          slug    => router path name
          icon    => Feather Icon component/icon name
          tag     => text to display on badge
          tagColor  => class to apply on badge element
          i18n    => Internationalization
          submenu   => submenu of current item (current item will become dropdown )
                NOTE: Submenu don't have any icon(you can add icon if u want to display)
          isDisabled  => disable sidebar item/group
  ----------------------------------------------------------------------------------------
  Item Name: Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template
  Author: Pixinvent
  Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/


export default [
  
  {
    header: 'Apps',
    icon: 'PackageIcon',
    i18n: 'Apps',
    items: [
   

  
      {
        url: null,
        name: 'User',
        icon: 'UserIcon',
        i18n: 'User',
        submenu: [
          {
            url: '/apps/user/user-list',
            name: 'List',
            slug: 'app-user-list',
            i18n: 'List'
          }
          // {
          //   url: '/apps/user/user-view/268',
          //   name: 'View',
          //   slug: 'app-user-view',
          //   i18n: 'View'
          // },
          // {
          //   url: '/apps/user/user-edit/268',
          //   name: 'Edit',
          //   slug: 'app-user-edit',
          //   i18n: 'Edit'
          // }
        ]
      }
    ]
  },

  {
    header: 'Others',
    icon: 'MoreHorizontalIcon',
    i18n: 'Others',
    items: [
      {
        url: null,
        name: 'Menu Levels',
        icon: 'MenuIcon',
        i18n: 'MenuLevels',
        submenu: [
          {
            url: null,
            name: 'Menu Level 2.1',
            i18n: 'MenuLevel2p1'
          },
          {
            url: null,
            name: 'Menu Level 2.2',
            i18n: 'MenuLevel2p2',
            submenu: [
              {
                url: null,
                name: 'Menu Level 3.1',
                i18n: 'MenuLevel3p1'
              },
              {
                url: null,
                name: 'Menu Level 3.2',
                i18n: 'MenuLevel3p2'
              }
            ]
          }
        ]
      },
      {
        url: null,
        name: 'Disabled Menu',
        icon: 'EyeOffIcon',
        i18n: 'DisabledMenu',
        isDisabled: true
      },
      {
        url: null,
        name: 'Support',
        icon: 'SmileIcon',
        i18n: 'Support',
        submenu: [
          {
            url: 'https://pixinvent.com/demo/vuexy-vuejs-admin-dashboard-template/documentation/',
            name: 'Documentation',
            icon: 'BookOpenIcon',
            slug: 'external',
            i18n: 'Documentation',
            target: '_blank'
          },
          {
            url: 'https://pixinvent.ticksy.com/',
            name: 'Raise Support',
            icon: 'LifeBuoyIcon',
            slug: 'external',
            i18n: 'RaiseSupport',
            target: '_blank'
          }
        ]
      }
    ]
  }
]

