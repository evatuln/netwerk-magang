<?php
$sid = isset($_GET['sid']) ? intval(trim($_GET['sid'])) : 0;
$next = $sid ? 'redeem-voucher.php?sid=' . $sid : 'redeem-voucher.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" href="assets/img/favicon.ico">
	<title>CemCem Snack Pilus - Home</title>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
	<link rel="stylesheet" href="assets/css/main.css">
    <script>
        const BASE_URL = 'https://surprize.asia/cemcem';
        const BASE_URL_API = 'https://surprize.asia/api/cemcem';
    </script>
</head>
<body>

	<main role="main" class="bg-white">
		<section class="head" style="padding-bottom: 0;">
			<?php include 'logo-head.php';?>
			<div class="container-fluid">
				<div class="row">
					<div class="banner">
						<img src="assets/img/banner_cemcem.png" alt="" class="img-fluid">
					</div>
				</div>
			</div>
		</section>
		<section class="bg-blue section-cta">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12 text-center">
						<p>Cukup Scan QR dan dapatkan CEMCEM Snack Pilus GRATIS!
     Ayo buruan selama persediaan masih ada.</p>
						<a href="<?php echo $next; ?>" class="btn red thin">Redeem Sekarang</a>
					</div>
				</div>
			</div>
		</section>
		<section class="bg-white">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12">
						<h2 class="section-title text-center">Isi Data Diri & Dapatkan :</h2>
						<div class="content-wrap no-shadow text-center bd-gray detail-item">
							<img src="assets/img/logo-beon.png" alt="" class="img-fluid">
							<p>GRATIS! CEMCEM Snack Pilus berbagai plihan rasa.</p><br/>
							<p>Camilan ideal untuk menemani saat-saat santai Anda,
     dibuat dengan telur, ikan dan keju.</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section>
			<div class="container-fluid merchant">
				<div class="row">
					<div class="col-10 offset-1 text-center">
						<h2 class="section-title">Merchant</h2>
						<div class="row">
							<div class="col text-center"><img src="assets/img/logo_kioswarga.jpeg" alt="" class="img-fluid logo"></div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="bg-blue section-cta">
			<div class="container-fluid">
				<div class="row">
					<div class="col-12 text-center">
						<p>Dapatkan CEMCEM Snack Pilus Gratis, cukup Scan QR.</p>
						<a href="<?php echo $next; ?>" class="btn red thin">Redeem Sekarang!</a>
					</div>
				</div>
			</div>
		</section>
	</main>
	<link rel="stylesheet" href="assets/css/footer.css">
	<footer class="footer text-center bg-white">
		<a href="#">Disclaimer @ Ownership</a>
		<p class="gray">Copyright 2020, Surprize Asia</p>
		<!-- <p class="gray">AXA Indonesia merupakan perusahaan yang terdaftar dan diawasi oleh Otoritas Jasa Keuangan.</p> -->
	</footer>

	<script src="https://code.jquery.com/jquery-3.4.1.min.js"  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="  crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/main.js"></script>
    <script>
        localStorage.setItem('cemcem-campaignId', 12);
    </script>
</body>
</html>