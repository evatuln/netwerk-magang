<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="icon" href="assets/img/favicon.ico">
	<title>CemCem Snack Pilus - e-Voucher</title>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
	<link rel="stylesheet" href="assets/css/main.css">
	<link rel="stylesheet" href="assets/css/voucher.css">
    <script>
        const BASE_URL = 'https://surprize.asia/cemcem';
        const BASE_URL_API = 'https://surprize.asia/api/cemcem';

        const urlParams = new URLSearchParams(window.location.search);
        const voucher = urlParams.get('v');
    </script>
</head>
<body>

	<main role="main">
		<section class="voucher-wrapper">
			<?php include 'logo-head.php';?>
			<div class="container-fluid">
				<div class="row">
					<div class="col-12">
						<h2 class="text-center cl-blue">Selamat!</h2>
						<div class="profile-detail text-center">
							<p class="name">(user fullname)</p>
							<p class="email">(user email)</p>
							<p class="phone-number">(user phone)</p>
						</div>
						<div class="announce text-center">
							<p>Anda Mendapatkan</p>
							<p>CEMCEM Snack Pilus</p>
                            <p style="margin-bottom:15px !important;"><b>GRATIS!</b></p>
                            <p style="font-size:16px !important;color:#292929 !important;">Cek pesan WhatApp Anda sekarang.</p>
                            <p style="font-size:16px !important;color:#292929 !important;">Tap URL/Link pada pesan WhatsApp untuk menukarkan e-Voucher dengan Hadiah di toko terdekat, selama persediaan masih ada.</p>
							<a href="#">* Syarat & Ketentuan</a>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>
	<?php //include 'social.php';?>
	<?php include 'footer.php';?>

	<script src="https://code.jquery.com/jquery-3.4.1.min.js"  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="  crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="assets/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
	<script src="assets/js/main.js"></script>
    <script>
        let campaignId = localStorage.getItem('cemcem-campaignId');
        if (typeof campaignId === 'undefined' || !campaignId) {
            campaignId = 12;
        }
        const userId = localStorage.getItem('cemcem-userId');

        $.ajax({
            url: `${BASE_URL_API}/campaign/voucher/${campaignId}/claim?_ticket=${userId}`,
            cache: false,
            success: function(result){
                const { name, email, phone } = result.data;

                $("p.name").text(name);
                $("p.email").text(email);
                $("p.phone-number").text(phone);
            },
            error: function(err){
                if (err.status == 404){
                    window.location.replace(`${BASE_URL}/404-not-found.php`);

                    return;
                }

                $.alert({
                    title: 'Error!',
                    content: err.responseJSON.message,
                });
            },
        })
    </script>
</body>
</html>